'use strict';

var gulp = require('gulp'),
    git = require('gulp-git'),
    rename = require("gulp-rename"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    paths = {
        js: 'lib/*.js'
    };

module.exports = {
    default: _distJS
};

function _distJS() {
    return gulp.src(paths.js)
        .pipe(concat('sp-in-view-watcher.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename('sp-in-view-watcher.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}